#region [#### Variables declariation]
$arrSubscriptionName            = "Visual Studio Enterprise with MSDN"
$arrTemplatesRG                 = "arrarmprjs"
$arrTemplatesStorageAcct        = "arrtemplatesstgacct"
$arrTemplatesContainer          = "linuxmanageddiskvm"
$arrTemplatesUri                = "https://$arrTemplatesStorageAcct.blob.core.windows.net/$arrTemplatesContainer"

$arrLocation                    = "Westeurope"
$arrResourceGroupName           = "arrlvmmngtdisk"
$basedir                        = "C:\GIT\azureManagedDiskLinuxVm" 
$arrTemplanteName               = "$basedir\template.json"
$arrTemplanteParameters         = "$basedir\parameters.json"
#$masterTemplateFile             = "$basedir\masterTemplateAndLinuxCustExt.json"
$masterTemplateFile             = "$basedir\masterTemplateAndEmpty.json"
$masterTemplateParametersFile   = "$baseDir\masterTemplate.parameters.json"
$customScriptName               = "customscript.sh"
#endregion

#region [#### Login to Azure, set the Context and working directory]
# Checks if the login to Azure has already been done
try
{
    Get-AzureRmContext | out-null
} # use the code "$Error[0] | fl * -Force" to find the exception to catch
catch [System.Management.Automation.PSInvalidOperationException]
{
    Write-Host "Not Logged in"
    $loggedIn = $false
}
finally
{
    If (!$loggedIn) {
        Login-AzureRmAccount
        $loggedIn = $true
    }
    $accountId = (Get-AzureRmContext).Account.id
    Write-Host "You are logged in to Azure as: $accountId"
}

## Set the Azure Context
Set-AzureRmContext -SubscriptionName $arrSubscriptionName
Set-Location $basedir
#endregion

#region [#### Create the Resource Group - forces the recreation if the RG already exists]
# Create or update a resource group for this specific project 
New-AzureRmResourceGroup -Name $arrResourceGroupName `
                         -Location $arrLocation `
                         -Force `
                         -ErrorAction Stop `
                         -Verbose 
#endregion

#region [#### upload templates]

Set-Location $baseDir
$StorageAccountContext = (Get-AzureRmStorageAccount -ResourceGroupName $arrTemplatesRG | 
	                       Where-Object{$_.StorageAccountName -eq $arrTemplatesStorageAcct}).Context
Get-ChildItem | 
	Set-AzureStorageBlobContent -Context $StorageAccountContext `
	                            -Container $arrTemplatesContainer.ToLower() `
								-Force
#endregion

#region [#### Launch the Master Template]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $masterTemplateFile).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $masterTemplateFile `
                                   -TemplateParameterFile $masterTemplateParametersFile `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $masterTemplateFile `
#                                    -TemplateParameterFile $masterTemplateParametersFile `
#                                    -verbose

#endregion




#### tests
#region [#### Launch Single Deployments]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem "$basedir\AddNsgRules.json").BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile "$basedir\AddNsgRules.json" `
                                   -TemplateParameterFile "$basedir\AddNsgRules.parameters.json" `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile "$basedir\addNsgRules.json" ` 
#                                    -TemplateParameterFile "$basedir\AddNsgRules.parameters.json"  

#endregion

### still needed ?

#region [#### Launch the centOSsftp Deployment]

$deploymentResult = `
New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $arrTemplanteName).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $arrTemplanteName `
                                   -TemplateParameterFile $arrTemplanteParameters `
                                   -Force `
                                   -Verbose

## Use $deploymentResult.Outputs.connectionCommand.Value to get the command for sftp the server

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $arrTemplanteName `
#                                    -TemplateParameterFile $arrTemplanteParameters 

#endregion

#region [#### Launch the Linux Customscript Deployment]
#$customScriptName = "createuser.sh"
$key = Get-AzureRmStorageAccountKey -StorageAccountName $arrTemplatesStorageAcct -ResourceGroupName $arrTemplatesRG
$context = New-AzureStorageContext -StorageAccountName $arrTemplatesStorageAcct -StorageAccountKey $key[0].Value
$sasCustomScript = New-AzureStorageBlobSASToken -Context $context -Container $arrTemplatesContainer.ToLower() -Blob $customScriptName -Permission rl
#$sasCustomScript = ""
New-AzureRmResourceGroupDeployment  -ResourceGroupName $arrResourceGroupName `
                                    -Name ((Get-ChildItem $arrTemplanteName).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                    -TemplateFile "$basedir\CustScriptExtension.json" `
                                    -TemplateParameterFile "$basedir\CustScriptExtension.andrea.parameters.json" `
									-scriptSas $sasCustomScript `
									-masterOrps "ps" `
									-username "pat" `
									-sftpport 2222 `
                                    -Force `
                                    -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile "$basedir\CustScriptExtension.json" `
#                                    -TemplateParameterFile "$basedir\CustScriptExtension.parameters.json" 

#endregion

#region [#### Remove the Resource Group]
#Remove-AzurermResourceGroup -Name $arrResourceGroupName -Force
#endregion

#region [#### notes]
## $t = Get-Content centOSsftp.json | ConvertFrom-Json
## $t.resources.properties.osProfile.adminUsername 
##
## SFTP Chroot Jail
## https://community.spiceworks.com/how_to/112551-setup-a-sftp-server-running-on-centos-linux-7
##
## Custom script logs at /var/log/azure/
## and at /var/lib/waagent/custom-script/download/XXX
##
## Check selinux status
## sestatus
## Check the selinux policies for sh
## getsebool -a | grep 'ssh'
## Check the open ports
## ss -tnlp | grep ssh
##
## Could not add a null sas key at the end of the customscript uri for when I call the template
## from the maser template, so I used the conditional hash variable masterOrps to have 2 URIs
## 
## Template variables are case sensitive therfore template and parameters files must match the case
##
## Running Azure automation with an MSDN Subscription
## https://blogs.technet.microsoft.com/tip_of_the_day/2016/06/24/cloud-tip-of-the-day-running-azure-automation-with-an-msdn-subscription/
##
## to create a strong linux encrypted password
## http://backreference.org/2014/04/19/many-ways-to-encrypt-passwords/
## http://unix.stackexchange.com/questions/158400/etc-shadow-how-to-generate-6-s-encrypted-password
##
## http://harvestingclouds.com/post/step-by-step-arm-templates-deploying-template-using-azure-powershell/
## see the key gotchas in particular 'All inline parameters are ignored when you specify "TemplateParameterUri" parameter.'
##
#endregion

#region [#### two git repos]

git pull https://gitlab.com/rzand/sftpserver.git master

git pull https://talktalktv.visualstudio.com/TV/Squad-TechEnablement/_git/Ops-sftp master

git push https://gitlab.com/rzand/sftpserver.git master

git push https://talktalktv.visualstudio.com/TV/Squad-TechEnablement/_git/Ops-sftp master

# git pull https://ttarr.visualstudio.com/DefaultCollection/_git/ttsftp master
# git push https://ttarr.visualstudio.com/DefaultCollection/_git/ttsftp master

#to add the vso repo
git remote add origin https://ttarr.visualstudio.com/DefaultCollection/_git/ttsftp
git push -u origin --all

git remote rm origin

git remote add origin https://talktalktv.visualstudio.com/TV/Squad-TechEnablement/_git/Ops-sftp
git push -u origin --all

# credential for talttalktv vso ttarr F6 - ttarr vso arrvso F6 

#endregion

#region [#### Create the resource group, the storage account and automation account]
# Create or update a resource group for this specific project 
New-AzureRmResourceGroup -Name $arrResourceGroupName `
                         -Location $arrLocation `
                         -Force `
                         -ErrorAction Stop `
                         -Verbose 

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $accountTemplateFile).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $accountTemplateFile `
                                   -TemplateParameterFile $accountTemplateParametersFile `
                                   -Force `
                                   -Verbose


## Create a Storage Account 
#$stgAlreadyExist = $null
#$stgAlreadyExist = Get-AzureRmStorageAccount -ResourceGroupName $arrResourceGroupName `
#                                             -Name $arrStorageAcctName `
#											 -ErrorAction Ignore

#if ($stgAlreadyExist -eq $null) {
#	New-AzureRmStorageAccount -ResourceGroupName $arrResourceGroupName `
#							  -Name $arrStorageAcctName.ToLower() `
#							  -Location $arrLocation `
#							  -Type $arrStorAcctRplType
#							   }

## Create the Azure Automation Account
#$automationaccount = `
#New-AzureRmAutomationAccount -ResourceGroupName $arrResourceGroupName `
#                             -Name $arrAutomationAcctName `
#                             -Location $arrAALocation `
#                             -Verbose

## N.B.: The location for the Automation Account is different from the location of the other objects because 
## at the time I am writing this Powershell (Nov 2016) the Microsoft.Automation is not available in ukwest
## to verifu use the command 
## (Get-AzureRmLocation | where location -eq ukwest).providers

#endregion

#region [#### Create the Automation RunAs user]

Set-Location $basedir
$SelfSignedCertPlainPassword = Read-Host "Password for the Self Signed Cert"
./New-RunAsAccount.ps1 -ResourceGroup $arrResourceGroupName `
                       -AutomationAccountName $arrAutomationAcctName `
					   -ApplicationDisplayName "$($arrAutomationAcctName)RunAs" `
					   -SubscriptionId $arrSubscriptionID `
					   -CreateClassicRunAsAccount $false `
					   -SelfSignedCertPlainPassword $SelfSignedCertPlainPassword

#endregion



