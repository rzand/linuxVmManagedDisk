﻿#region [#### Variables declariation]
$arrSubscriptionName            = "Visual Studio Enterprise with MSDN"
$arrTemplatesRG                 = "arrarmprjs"
$arrTemplatesStorageAcct        = "arrtemplatesstgacct"
$arrTemplatesContainer          = "linuxmanageddiskvm"
$arrTemplatesUri                = "https://$arrTemplatesStorageAcct.blob.core.windows.net/$arrTemplatesContainer"

$arrLocation                    = "Westeurope"
$arrResourceGroupName           = "arrlvmmngtdisk"
$basedir                        = "C:\GIT\azureManagedDiskLinuxVm" 
$arrTemplanteName               = "$basedir\template.json"
$arrTemplanteParameters         = "$basedir\parameters.json"
$masterTemplateFile             = "$basedir\masterTemplateAndLinuxCustExt.json"
$masterTemplateParametersFile   = "$baseDir\masterTemplate.parameters.json"
$customScriptName               = "customscript.sh"
#endregion

#region [#### Login to Azure, set the Context and working directory]
# Checks if the login to Azure has already been done
try
{
    Get-AzureRmContext | out-null
} # use the code "$Error[0] | fl * -Force" to find the exception to catch
catch [System.Management.Automation.PSInvalidOperationException]
{
    Write-Host "Not Logged in"
    $loggedIn = $false
}
finally
{
    If (!$loggedIn) {
        Login-AzureRmAccount
        $loggedIn = $true
    }
    $accountId = (Get-AzureRmContext).Account.id
    Write-Host "You are logged in to Azure as: $accountId"
}

## Set the Azure Context
Set-AzureRmContext -SubscriptionName $arrSubscriptionName
Set-Location $basedir
#endregion

#region [#### Create the Resource Group - forces the recreation if the RG already exists]
# Create or update a resource group for this specific project 
New-AzureRmResourceGroup -Name $arrResourceGroupName `
                         -Location $arrLocation `
                         -Force `
                         -ErrorAction Stop `
                         -Verbose 
#endregion

#region [#### upload templates]

Set-Location $baseDir
$StorageAccountContext = (Get-AzureRmStorageAccount -ResourceGroupName $arrTemplatesRG | 
	                       Where-Object{$_.StorageAccountName -eq $arrTemplatesStorageAcct}).Context
Get-ChildItem | 
	Set-AzureStorageBlobContent -Context $StorageAccountContext `
	                            -Container $arrTemplatesContainer.ToLower() `
								-Force
#endregion

#region [#### Launch the Master Template]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $masterTemplateFile).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $masterTemplateFile `
                                   -TemplateParameterFile $masterTemplateParametersFile `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $masterTemplateFile `
#                                    -TemplateParameterFile $masterTemplateParametersFile `
#                                    -verbose

#endregion

#region [#### Remove the Resource Group]
#Remove-AzurermResourceGroup -Name $arrResourceGroupName -Force
#endregion
